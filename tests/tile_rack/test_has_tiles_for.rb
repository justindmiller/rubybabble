#Test cases for has_tiles_for method in TileRack

require "minitest/autorun"
require_relative "../../tile_rack.rb"

class TestHasTilesFor < Minitest::Test
	
	#Sets up a basic TileRack to be tested on
	def setup
		@testrack = TileRack.new
	end

	#checks if rack has needed letters when letters are in order with no duplicates
	def test_rack_has_needed_letters_when_letters_are_in_order_no_duplicates
		@testrack.append(:C)
		@testrack.append(:A)
		@testrack.append(:B)
		assert_equal true, @testrack.has_tiles_for?("CAB")
	end

	#test rack has letters when letters are not in order, no duplicates
	def test_rack_has_needed_letters_when_letters_are_not_in_order_no_duplicates
		@testrack.append(:A)
		@testrack.append(:B)
		@testrack.append(:C)
		assert_equal true, @testrack.has_tiles_for?("CAB")
	end

	#rack has no needed letters
	def test_rack_doesnt_contain_any_needed_letters
		@testrack.append(:C)
		@testrack.append(:A)
		@testrack.append(:B)
		assert_equal false, @testrack.has_tiles_for?("YES")
	end

	#rack contains some but not all needed letters
	def test_rack_contains_some_but_not_all_needed_letters
		@testrack.append(:A)
		@testrack.append(:N)
		@testrack.append(:D)
		assert_equal false, @testrack.has_tiles_for?("SAND")
	end

	#test rack has a word with duplicate letters
	def test_rack_contains_a_word_with_duplicate_letters
		@testrack.append(:A)
		@testrack.append(:A)
		@testrack.append(:N)
		@testrack.append(:D)
		assert_equal true, @testrack.has_tiles_for?("AND")
	end

	#test rack dosent contain enough duplicate letters
	def test_rack_doesnt_contain_enough_duplicate_letters
		@testrack.append(:S)
		@testrack.append(:E)
		@testrack.append(:N)
		assert_equal false, @testrack.has_tiles_for?("SEEN")
	end
end
