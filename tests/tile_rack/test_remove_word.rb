#Test cases for remove_word method in TileRack

require "minitest/autorun"
require_relative "../../tile_rack.rb"

class TestRemoveWord < Minitest::Test

	#setup method for class
	def setup
		@controlrack = TileRack.new
		@testrack = TileRack.new		
	end

	#Can remove a word whose letters are in order
	def test_can_remove_a_word_whose_letters_are_in_order_on_the_rack
		@controlrack.append(:S)
		@controlrack.append(:Z)
		@controlrack.append(:P)
		@testrack.append(:L)
		@testrack.append(:A)
		@testrack.append(:T)
		@testrack.append(:E)
		@testrack.append(:S)
		@testrack.append(:Z)
		@testrack.append(:P)
		@testrack.remove_word("LATE")
		assert_equal @controlrack.tiles.sort, @testrack.tiles.sort		
	end

	#Can remove a word whose letters are not in order on the rack
	def test_can_remove_a_word_whose_letters_are_not_in_order_on_the_rack
		@controlrack.append(:Z)
		@controlrack.append(:P)
		@controlrack.append(:S)		
		@testrack.append(:A)
		@testrack.append(:L)
		@testrack.append(:E)
		@testrack.append(:T)
		@testrack.append(:Z)
		@testrack.append(:S)
		@testrack.append(:P)
		@testrack.remove_word("LATE")
		assert_equal @controlrack.tiles.sort, @testrack.tiles.sort
	end

	#Can remove a word with duplicate letters
	def test_can_remove_word_with_duplicate_letters
		@controlrack.append(:D)
		@controlrack.append(:M)
		@controlrack.append(:U)
		@testrack.append(:D)
		@testrack.append(:M)
		@testrack.append(:U)
		@testrack.append(:S)
		@testrack.append(:E)
		@testrack.append(:E)
		@testrack.append(:N)
		@testrack.remove_word("SEEN")
		assert_equal @controlrack.tiles.sort, @testrack.tiles.sort
	end

	#Can remove a word with duplicate letters without removing unneeded duplicate letters
	def test_can_remove_word_without_removing_unneeded_duplicate_letters
		@controlrack.append(:S)
		@controlrack.append(:M)
		@controlrack.append(:E)
		@testrack.append(:S)
		@testrack.append(:M)
		@testrack.append(:E)
		@testrack.append(:S)
		@testrack.append(:E)
		@testrack.append(:E)
		@testrack.append(:N)
		@testrack.remove_word("SEEN")
		assert_equal @controlrack.tiles.sort, @testrack.tiles.sort
	end
end
