#Test cases for number_of_tiles_needed method in TileRack

require "minitest/autorun"
require_relative "../../tile_rack.rb"

class TestNumberOfTilesNeeded < Minitest::Test

	#setup method for class
	def setup
		@testrack = TileRack.new
	end
	
	#will test when the tile rack is empty
	def test_empty_tile_rack_should_need_max_tiles
		assert_equal 7, @testrack.number_of_tiles_needed
	end

	#will need max tiles minus one
	def test_tile_rack_with_one_tile_should_need_max_minus_one_tiles
		@testrack.append(:a)
		assert_equal 6, @testrack.number_of_tiles_needed
	end

	#will test a more general situation with several tiles needed
	def test_tile_rack_with_several_tiles_should_need_some_tiles
		@testrack.append(:a)
		@testrack.append(:b)
		@testrack.append(:c)
		assert_equal 4, @testrack.number_of_tiles_needed
	end

	#make sure a full rack won't need tiles
	def test_that_full_tile_rack_doesnt_need_any_tiles
		@testrack.append(:a)
		@testrack.append(:b)
		@testrack.append(:c)
		@testrack.append(:d)
		@testrack.append(:e)
		@testrack.append(:f)
		@testrack.append(:g)
		assert_equal 0, @testrack.number_of_tiles_needed
	end
end
