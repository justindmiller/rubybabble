#Class TestDrawTile
#unit tests for the TileBag#draw_tile method
#will test if proper number of tiles are in bag, that bag will flag as empty, and that the tile distribution is correct

require "minitest/autorun"
require_relative "../../tile_bag.rb"

class TestDrawTile < Minitest::Test
 
 #Setup method for the test cases
 #creates a default instance variable @testbag
 def setup
	@testbag = TileBag.new
 end

 #test_has_proper_number_of_tiles, tests that exactly 98 tiles can be drawn
 #after which bag is empty
 #will draw 98 times from the bag, and then assert that the bag is empty
 def test_has_proper_number_of_tiles
	98.times {@testbag.draw_tile}
	assert_equal true, @testbag.empty?
 end

 #test_has_proper_tile_distribution, tests that the tiles in the bag are distributed
 #exactly as specified in the Scrabble rules
 #will compare 
 def test_has_proper_tile_distribution
	requiredtiles = {
	:E => 12, 
	:A => 9, :I => 9, 
	:O => 8,
	:N => 6,:R => 6,:T => 6,
	:L => 4,:S => 4,:U => 4,:D => 4,
	:G => 3,
	:B => 2,:C => 2,:M => 2,:P => 2,:F => 2,
	:H => 2,:V => 2,:W => 2,:Y => 2,
	:K => 1,:J => 1,:X => 1,:Q => 1,:Z => 1
	}

	currenttiles = {
	:E => 0, 
	:A => 0, :I => 0, 
	:O => 0,
	:N => 0,:R => 0,:T => 0,
	:L => 0,:S => 0,:U => 0,:D => 0,
	:G => 0,
	:B => 0,:C => 0,:M => 0,:P => 0,:F => 0,
	:H => 0,:V => 0,:W => 0,:Y => 0,
	:K => 0,:J => 0,:X => 0,:Q => 0,:Z => 0
	}

	98.times {currenttiles[@testbag.draw_tile]+=1}

	assert_equal requiredtiles, currenttiles	
 end
end
