#Test cases for remove method of TileGroup

require "minitest/autorun"
require_relative "../../tile_group.rb"

class TestRemove < Minitest::Test

	#setup method to define a basic array of symbols to be tested on
	def setup
		@controlgroup = TileGroup.new
		@testgroup = TileGroup.new
	end
	
	#Will test basic single remove
	def test_remove_one_tile
		@testgroup.append(:a)
		@testgroup.remove(:a)
		assert_equal @controlgroup.tiles, @testgroup.tiles	
	end

	#Will test removing only the first tile from many tiles
	def test_remove_first_tile_from_many
		@controlgroup.append(:b)
		@controlgroup.append(:c)
		@testgroup.append(:a)
		@testgroup.append(:b)
		@testgroup.append(:c)
		@testgroup.remove(:a)
		assert_equal @controlgroup.tiles, @testgroup.tiles
	end

	#Will test removing only the last tile
	def test_remove_last_tile_from_many
		@controlgroup.append(:a)
		@controlgroup.append(:b)
		@testgroup.append(:a)
		@testgroup.append(:b)
		@testgroup.append(:c)
		@testgroup.remove(:c)
		assert_equal @controlgroup.tiles, @testgroup.tiles
	end

	#Will test removing only the middle tile from many tiles
	def test_remove_middle_tile_from_many
		@controlgroup.append(:a)
		@controlgroup.append(:c)
		@testgroup.append(:a)
		@testgroup.append(:b)
		@testgroup.append(:c)
		@testgroup.remove(:b)
		assert_equal @controlgroup.tiles, @testgroup.tiles	
	end

	#Will test removing multiple tiles at once
	def test_remove_multiple_tiles
		@controlgroup.append(:a)
		@testgroup.append(:a)
		@testgroup.append(:b)
		@testgroup.append(:c)
		@testgroup.remove(:b)
		@testgroup.remove(:c)
		assert_equal @controlgroup.tiles, @testgroup.tiles
	end

	#test to make sure duplicate tiles are not removed
	def test_make_sure_duplicates_are_not_removed
		@controlgroup.append(:a)
		@controlgroup.append(:c)
		@testgroup.append(:a)
		@testgroup.append(:a)
		@testgroup.append(:c)
		@testgroup.remove(:a)
		assert_equal @controlgroup.tiles, @testgroup.tiles	
	end	

end
