#Test cases for Append method of TileGroup

require "minitest/autorun"
require_relative "../../tile_group.rb"

class TestAppend < Minitest::Test

	#setup method to define a basic array of symbols to be tested on
	def setup
		@testgroup = TileGroup.new
	end
	
	#Will test basic single append
	def test_append_one_tile
		controlgroup = [:a]
		@testgroup.append(:a)			
		assert_equal controlgroup, @testgroup.tiles
	end

	#Will test appending many tiles
	def test_append_many_tiles
		controlgroup = [:a,:b,:c]
		@testgroup.append(:a)
		@testgroup.append(:b)
		@testgroup.append(:c)			
		assert_equal controlgroup, @testgroup.tiles
	end

	#Will test appending duplicate tiles
	def test_append_duplicate_tiles
		controlgroup = [:a,:a,:c]
		@testgroup.append(:a)
		@testgroup.append(:a)
		@testgroup.append(:c)			
		assert_equal controlgroup, @testgroup.tiles
	end

end
