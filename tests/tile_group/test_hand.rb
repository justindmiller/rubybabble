#Test cases for hand method of TileGroup

require "minitest/autorun"
require_relative "../../tile_group.rb"

class TestHand < Minitest::Test

	#setup method to define a basic array of symbols to be tested on
	def setup
		@testgroup = TileGroup.new
	end
	
	#Will test empty hand
	def test_convert_empty_group_to_string
		controlstring = ""
		assert_equal controlstring, @testgroup.hand
	end

	#Will test single tile hand
	def test_convert_single_tile_group_to_string
		controlstring = "a"
		@testgroup.append(:a)
		assert_equal controlstring, @testgroup.hand
	end

	#Will test multi tile hand
	def test_convert_multi_tile_group_to_string
		controlstring = "ab"
		@testgroup.append(:a)
		@testgroup.append(:b)
		assert_equal controlstring, @testgroup.hand
	end

	#Will test multi tile hand with duplicates
	def test_convert_multi_tile_group_with_duplicates_to_string
		controlstring = "aab"
		@testgroup.append(:a)
		@testgroup.append(:a)
		@testgroup.append(:b)
		assert_equal controlstring, @testgroup.hand
	end
end
