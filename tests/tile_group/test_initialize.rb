#Test case for Initialize method of TileGroup

require "minitest/autorun"
require_relative "../../tile_group.rb"

class TestInitialize < Minitest::Test
	
	#Will test basic initialization with empty tile group
	def test_create_empty_tile_group
		controlarray = []
		testgroup = TileGroup.new
		testarray = testgroup.tiles
		assert_equal controlarray, testarray
	end

end
