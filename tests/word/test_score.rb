#Test case for Score method of TileWord

require "minitest/autorun"
require_relative "../../word.rb"

class TestScore < Minitest::Test

	#setup method to define a basic array of symbols to be tested on
	def setup
		@testword = Word.new
	end
	
	#Will test basic empty word with score of zero
	def test_empty_word_should_have_score_of_zero
		assert_equal 0, @testword.score
	end
	
	#Will test basic one tile word score
	def test_score_a_one_tile_word
		@testword.append(:X)
		assert_equal 8, @testword.score
	end
	
	#Will test score pf word with multiple different tiles
	def test_score_a_word_with_multiple_different_tiles
		@testword.append(:C)
		@testword.append(:D)
		assert_equal 5, @testword.score
	end
	
	#Will test score a word with recurring tiles
	def test_score_a_word_with_recurring_tiles
		@testword.append(:X)
		@testword.append(:X)
		@testword.append(:X)
		assert_equal 24, @testword.score
	end

end
