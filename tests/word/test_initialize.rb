#Test case for Initialize method of TileWord

require "minitest/autorun"
require_relative "../../word.rb"

class TestInitialize < Minitest::Test
	
	#Will test basic initialization with empty word group
	def test_create_empty_word
		testword = Word.new
		assert_equal 0, testword.score
	end

end
