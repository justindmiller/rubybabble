#Parent class that handles groups of tiles
class TileGroup 

	#initialize method to setup class
	def initialize
		@tiles = Array.new
	end

	#appends a tile to the group
	def append(tile)
		@tiles.push(tile)
	end

	#acessor method that returns tiles in group
	def tiles
		return @tiles
	end

	#removes a tile from the group
	def remove(tile)
		@tiles.delete_at(@tiles.find_index(tile))
	end

	#returns a string that is a concentation of all tiles string values
	def hand	
		concentation = ""	
		@tiles.each{|x|concentation += x.to_s}
		return concentation
	end

end
