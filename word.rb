#A class that handles a scoreable group of tiles. Child class of TileGroup

require_relative "tile_bag.rb"
require_relative "tile_group.rb"

class Word < TileGroup

	#class constructor, zero arguments
	def initialize()
		super
	end

	#returns the score of all points fot the tile this word contains
	def score
		sum = 0
		self.tiles.each{|x|sum += TileBag.points_for(x)}
		return sum
	end

end
