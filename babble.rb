#Driver class for the Ruby based Babble application

require_relative "tile_group.rb"
require_relative "tile_bag.rb"
require_relative "tile_rack.rb"
require_relative "word.rb"
require 'spellchecker'
require 'tempfile'

class Babble
	
	#Zero parameter constructor to create Babble objects
	def initialize
		@bag = TileBag.new
		@rack = TileRack.new
		@currentscore = 0
	end

	# the "main()" method (NOT STATIC!) that contains the main program loop that
	#* displays the letters in the tile rack
	#* prompts the user to guess a word
	#* if ":quit" is entered, displays "thanks for playing, total score: ACTUALSTORE
	#* if the word is not valid (fails spellcheck), displays "not a valid word"
	#* if the word is valid but the rack doesn't have the needed tiles, displays "Not enough tiles"
	#* if the word is valid and has the proper tiles, displays "You made WORD for X points"
	#* At the end of every guess, regardless of whether it was right or not, the current total score is displayed
	def run
		stop = false
		puts "Welcome to Babble!\n"
		puts "Make a word from the letters displayed!"
		puts "You may quit at any time by typing :QUIT"		
		until stop
			self.refresh_tile_rack
			puts "Current score: " + @currentscore.to_s + "\n"
			puts "Current hand: " + @rack.hand + "\n"
			puts "Word?>\n"
			userword = gets
			userword = userword.chomp.upcase
			switch = self.check_candidate_word(userword)
			case switch
			when 2
				puts "Not enough tiles"
			when 1
				puts "not a valid word"
			when 0
				stop = true
				puts "thanks for playing, total score: " + @currentscore.to_s
			else
				puts "You made " + userword + " for " + self.word_scores(userword) + " points\n"
			end
			if @bag.empty?
				stop = true
				puts "game is over! thanks for playing!"
			end
		end
	end

	#performs a spellcheck on the user word, will return true or false
	def user_spellcheck(word)
		return Spellchecker::check(word)[0][:correct]
	end

	#Scores a sucessful word, adds it to the total score and returns the word score as a string
	def word_scores(userword)
		scoredword = @rack.remove_word(userword)
		@currentscore += scoredword.score
		return scoredword.score.to_s
	end

	#Populates a tile rack
	def refresh_tile_rack
		@rack.number_of_tiles_needed.times{@rack.append(@bag.draw_tile)}
	end

	#Checks the word and returns an int based on the result
	def check_candidate_word(text)
		if text == ':QUIT' 
			return 0
		elsif !self.user_spellcheck(text)
			return 1
		elsif !@rack.has_tiles_for?(text)
			return 2
		else
			return 3
		end
	end
end

Babble.new.run
