#Child class of TileGroup, rack in which active tiles are worked on.

require_relative "tile_group.rb"
require_relative "word.rb"
require 'multiset'

class TileRack < TileGroup

	#will return the number of tiles needed to bring the rack back up to 7
	def number_of_tiles_needed
		return 7 - self.tiles.length
	end

	#returns true if the rack has enough tiles to make the word represented by the text
	#string parameter
	def has_tiles_for?(text)
		racktiles = Multiset.new(self.hand.each_char)
		newwordtiles = Multiset.new(text.each_char)
		return newwordtiles.subset?(racktiles)
	end

	#returns a Word object made by removing the tiles given by the string text
	def remove_word (text)
		scoreword = Word.new
		if self.has_tiles_for?(text)
			text.each_char{|symbol| scoreword.append(symbol.to_sym)}
			text.each_char{|symbol| self.remove(symbol.to_sym)}
		end
		return scoreword
	end

end
