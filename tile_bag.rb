# The program is a Scrabble like
# The class represents a bag of "tiles" that will go on a tile rack
#Author:: Justin Miller
class TileBag

 #initialize method that is a zero parameter that initializes a bag
 #with a complete set of tiles. Stores the set of tiles (as symbols) in an Array
 def initialize
	@arraybag = Array.new
	12.times {@arraybag.push(:E)}
	9.times {@arraybag.push(:A)}
	9.times {@arraybag.push(:I)}
	8.times {@arraybag.push(:O)}
	6.times {@arraybag.push(:N)}
	6.times {@arraybag.push(:R)}
	6.times {@arraybag.push(:T)}
	4.times {@arraybag.push(:L)}
	4.times {@arraybag.push(:S)}
	4.times {@arraybag.push(:U)}
	4.times {@arraybag.push(:D)}
	3.times {@arraybag.push(:G)}
	2.times {@arraybag.push(:B)}
	2.times {@arraybag.push(:C)}
	2.times {@arraybag.push(:M)}
	2.times {@arraybag.push(:P)}
	2.times {@arraybag.push(:F)}
	2.times {@arraybag.push(:H)}
	2.times {@arraybag.push(:V)}
	2.times {@arraybag.push(:W)}
	2.times {@arraybag.push(:Y)}
	@arraybag.push(:K)
	@arraybag.push(:J)
	@arraybag.push(:X)
	@arraybag.push(:Q)
	@arraybag.push(:Z)
 end

 #removes one random tile from the bag and returns it
 def draw_tile
	@arraybag.delete_at(rand(@arraybag.length))
 end

 #returns true if the bag is empty, false otherwise
 def empty?
	@arraybag.empty?
 end

 #this is a class method, tile parameter is a symbol representing a letter
 #the method returns the point for the given tile letter
 def self.points_for(tile)
	pointvalues = {
	:E => 1,
	:A => 1,
	:I => 1,
	:O => 1,
	:N => 1,
	:R => 1,
	:T => 1,
	:L => 1,
	:S => 1,
	:U => 1,
	:D => 2,
	:G => 2,
	:B => 3,
	:C => 3,
	:M => 3,
	:P => 3,
	:F => 4,
	:H => 4,
	:V => 4,
	:W => 4,
	:Y => 4,
	:K => 5,
	:J => 8,
	:X => 8,
	:Q => 10,
	:Z => 10
	}
	pointvalues[tile]
 end

end


